#!/bin/bash

CC="gcc"

CFLAGS="-O3 -D_FORTIFY_SOURCE=2 -I/usr/include/openssl"
LDFLAGS="-lcrypto -lm"

$CC ${CFLAGS} ${LDFLAGS} fortuna.c main.c -o fortuna

[ -x $(which strip) ] && strip fortuna