/**
  * Copyright 2018-Present leogx9r
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy 
  * of this software and associated documentation files (the "Software"), to 
  * deal in the Software without restriction, including without limitation the 
  * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
  * sell copies of the Software, and to permit persons to whom the Software is 
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in 
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
  * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
  * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
  * DEALINGS IN THE SOFTWARE.
  */

#ifndef __FORTUNA_H__
#define __FORTUNA_H__

#include <stdint.h>
#include <stddef.h>
#include <sys/time.h>

/**
  * Uses OpenSSL to provide SHA256 and AES.
  * If you rather provide your own methods, exclude these headers and define:
  *
  *   struct SHA256_CTX;
  *   struct AES_KEY;
  *
  *   void AES_set_encrypt_key( const uint8_t* keymaterial, size_t keybits, AES_KEY* state );
  *   void AES_encrypt( uint8_t* src, uint8_t* dst, AES_KEY* state );
  *
  *   void SHA256_Init( SHA256_CTX* ctx );
  *   void SHA256_Update( SHA256_CTX* ctx, uint8_t* src, size_t len );
  *   void SHA256_Final( uint8_t* digest, SHA256_CTX* ctx );
  *
  */
#include <openssl/aes.h>
#include <openssl/sha.h>

/**
  * Fortuna's specification indicates to use 32 pools.
  * Running out of entropy using this system will take approximately 13 years.
  */
#define FORTUNA_NUM_POOLS               32

/**
  * Reseeding time in microseconds. Equates to roughly 10x a second.
  */
#define FORTUNA_RESEED_TIME             100000

/**
  * After a single read chunk with 1MiB, force a reseed as per fortuna's specification.
  */
#define FORTUNA_MAX_READ                ( 1024 * 1024 )

/**
  * How many bytes is required for the initial seeding of fortuna.
  */
#define FORTUNA_MIN_SEED_CONTENT       32

/**
  * Set to 0 to disable assertions.
  */
#define FORTUNA_USE_ASSERTS            1

/**
  * Internally, this structure uses AES-256 in CTR mode and SHA-256 for updating the pools.
  * You **SHOULD** zero this structure out when done to avoid keeping the state in memory longer than necessary.
  */
struct fortuna_state {
    uint8_t             counter[ AES_BLOCK_SIZE ];

    size_t              reseeds;
    size_t              seeded;

    struct timeval      reseed_time;

    SHA256_CTX          pools[ FORTUNA_NUM_POOLS ];
    AES_KEY             cipher;
};

/* Initializes the internal state for use. */
void fortuna_init( struct fortuna_state* state );

/* Updates the internal state. This MUST be called at least once with at least 256-bits of input before use. */
void fortuna_add_entropy( struct fortuna_state* state, void* input, size_t input_length );

/* Extracts the specified amount of bytes from the state. */
void fortuna_extract( struct fortuna_state* state, void* output, size_t output_length );

#endif /* __FORTUNA_H__ */