/**
  * Copyright 2018-Present leogx9r
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy 
  * of this software and associated documentation files (the "Software"), to 
  * deal in the Software without restriction, including without limitation the 
  * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
  * sell copies of the Software, and to permit persons to whom the Software is 
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in 
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
  * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
  * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
  * DEALINGS IN THE SOFTWARE.
  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "fortuna.h"

void gather_entropy( uint8_t* buffer, size_t count ) {
    FILE* fp;
    size_t rc;

    /* Read from urandom instead because of how often seeds are done. ( 32 * 32 * 8 = 8192 bits ! ) */
    fp = fopen( "/dev/urandom", "r" );
    assert( fp != NULL );

    rc = fread( buffer, 1, count, fp );
    fclose( fp );

    /* Make sure we have the required length. */
    assert( rc == count );
}

/* Seeds the state with the minimum amount of entropy required. */
void seed( struct fortuna_state* state ) {
    uint8_t* e;

    /* Perform initialization if not already done. */
    if( !state->seeded )
        fortuna_init( state );

    /* Gather the minimum entropy needed to seed a pool. */
    e = malloc( FORTUNA_MIN_SEED_CONTENT );
    gather_entropy( e, FORTUNA_MIN_SEED_CONTENT );

    /* Insert the entropy into the k'th pool based on the internal reseed tick. */
    fortuna_add_entropy( state, e, FORTUNA_MIN_SEED_CONTENT );

    /* Be safe and avoid leaking the seed material. */
    memset( e, 0, FORTUNA_MIN_SEED_CONTENT );
    free( e );
}

/* Initializes Fortuna with enough entropy to produce cryptographically secure pseudo-random numbers. */
void main() {
    struct fortuna_state state;
    uint8_t buf[ FORTUNA_MAX_READ ];

    /* Ensure stdout is in binary mode and unbuffered. */
    stdout = freopen( NULL, "wb", stdout );
    setvbuf( stdout, NULL, _IONBF, 0 );

    /* Perform the initial seeding. */
    memset( &state, 0, sizeof( state ) );
    seed( &state );

    /* Extract till killed. */
    while( 1 ) {
        /* Extract in FORTUNA_MAX_READ byte chunks and output. */
        fortuna_extract( &state, buf, FORTUNA_MAX_READ );
        fwrite( buf, sizeof( uint8_t ), sizeof( buf ), stdout );
    }
}