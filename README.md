# Fortuna Implementation

    Fortuna is a cryptographically secure pseudorandom number generator (PRNG) 
    devised by Bruce Schneier and Niels Ferguson and published in 2003.

This implementation requires GCC and OpenSSL to be installed. Simply execute 
[build.sh](./build.sh) to build the binary `fortuna`.

The binary seeds itself initially from `/dev/urandom` using 256 bits of content 
then begins outputting cryptographically secure pseudo-random data to the 
console until the process is killed.

All code is licensed under the [MIT](./LICENSE).