/**
  * Copyright 2018-Present leogx9r
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy 
  * of this software and associated documentation files (the "Software"), to 
  * deal in the Software without restriction, including without limitation the 
  * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
  * sell copies of the Software, and to permit persons to whom the Software is 
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in 
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
  * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
  * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
  * DEALINGS IN THE SOFTWARE.
  */

#include <string.h>
#include <math.h>

#include "fortuna.h"

/* Use an empty macro to be compiled out if assertions are disabled. */
#if ( FORTUNA_USE_ASSERTS != 0 )
    #include <assert.h>
#else
    #define assert( x )
#endif

/**
  * Initialize the internal state and the pools.
  */
void fortuna_init( struct fortuna_state* state ) {
    size_t i;

    assert( state != NULL );
    memset( state, 0, sizeof( *state ) );

    /* Initialize each pool. */
    for( i = 0; i < FORTUNA_NUM_POOLS; i++ )
        SHA256_Init( &state->pools[ i ] );
}

/**
  * Determine if the state should be reseeded based on the last time it was done.
  */
int fortuna_should_reseed( struct fortuna_state* state ) {
    struct timeval cur;
    int flag;

    assert( state != NULL );

    /* Get the current time. */
    gettimeofday( &cur, NULL );

    flag = 0;

    /* Check if the reseed interval has passed. */
    if(
        ( cur.tv_sec > state->reseed_time.tv_sec + 1 ) ||
        ( cur.tv_usec - state->reseed_time.tv_usec >= FORTUNA_RESEED_TIME ) ||
        (
            cur.tv_sec == state->reseed_time.tv_sec + 1  &&
            ( 1000000 + cur.tv_usec - state->reseed_time.tv_usec >= FORTUNA_RESEED_TIME )
        )
    ) {
        /* Store the last updated time. */
        memcpy( &state->reseed_time, &cur, sizeof( cur ) );
        flag = 1;
    }

    memset( &cur, 0, sizeof( cur ) );

    return flag;
}

/**
  * Reseed the internal encryption key using the pools.
  */
void fortuna_reseed( struct fortuna_state* state ) {
    uint8_t digest[ SHA256_DIGEST_LENGTH ];
    SHA256_CTX ctx;
    size_t i, k;

    assert( state != NULL );

    /* The first and second reseeds use pool 0. */
    k = ++state->reseeds;

    SHA256_Init( &ctx );

    /* Use up to k-th pool 1/2**k of the time. */
    for( i = 0; i < FORTUNA_NUM_POOLS; i++ ) {
        SHA256_Final( digest, &state->pools[ i ] );

        /* Update the reseeding pool with all the i-th pool's content. */
        SHA256_Update( &ctx, digest, SHA256_DIGEST_LENGTH );

        if( !k || k & 1 )
            break;

        k >>= 1;
    }

    /* Calculate the final reseeding digest as the new key. */
    SHA256_Final( digest, &ctx );

    /* Update the encryption round keys now. */
    AES_set_encrypt_key( ( const uint8_t* )digest, SHA256_DIGEST_LENGTH * 8, &state->cipher );

    /* Ensure we reset the counter when reseeding. */
    memset( state->counter, 0, sizeof( state->counter ) );

    memset( &ctx, 0, sizeof( ctx ) );
    memset( digest, 0, sizeof( digest ) );
}

/**
  * Encrypts the current 128-bit counter to the destination buffer and increments it.
  */
void fortuna_update_counter( struct fortuna_state* state, void* dst ) {
    uint32_t* k;

    assert( state != NULL );
    assert( dst != NULL );

    /* Encrypt the current counter value into the destination. */
    AES_encrypt( state->counter, dst, &state->cipher );

    k = ( uint32_t* )state->counter;

    /* Increment the counter in 32-bit chunks. When it wraps around, we should reseed. */
    if( ++k[ 0 ] || ++k[ 1 ] || ++k[ 2 ] || ++k[ 3 ] )
        fortuna_reseed( state );
}

/**
  * Updates the cipher a new 128-bit key by encrypting the internal counter twice.
  */
void fortuna_update_key( struct fortuna_state* state ) {
    uint8_t key[ SHA256_DIGEST_LENGTH ];

    assert( state != NULL );

    /* Build a 256-bit block key by encrypting the current counter twice. */
    fortuna_update_counter( state, key );
    fortuna_update_counter( state, key + AES_BLOCK_SIZE );

    /* Recalculate the round keys for AES without resetting the counter. */
    AES_set_encrypt_key( ( const uint8_t* )key, SHA256_DIGEST_LENGTH * 8, &state->cipher );

    memset( key, 0, sizeof( key ) );
}

/**
  * Updates the entropy in all pools using the hash of the input provided.
  */
void fortuna_add_entropy( struct fortuna_state* state, void* input,
                         size_t input_length ) {
    uint8_t digest[ SHA256_DIGEST_LENGTH ];
    SHA256_CTX ctx;
    size_t k;

    assert( state != NULL );
    assert( input != NULL );
    assert( input_length != 0 );

    /* Check if this is the initial seed and make sure we have at least enough entropy. */
    if( !state->seeded || state->seeded < FORTUNA_MIN_SEED_CONTENT ) {
        /* Protect against overflow. */
        if( input_length > FORTUNA_MIN_SEED_CONTENT )
            state->seeded = FORTUNA_MIN_SEED_CONTENT;
        else
            state->seeded += input_length;

        /* Till we have enough entropy, keep mixing into pool 0. */
        state->reseeds = 0;
    }

    /* Calculate the digest of the input so we don't directly mix it in. */
    SHA256_Init( &ctx );
    SHA256_Update( &ctx, input, input_length );
    SHA256_Final( digest, &ctx );

    /**
      * On the n'th reseeding of the generator, pool k is used only if n is a multiple of 2**k.
      * Thus, the k'th pool is used only 1/2**k of the time.
      */
    if( !state->reseeds )
        k = 0;
    else {
        if( ( uint32_t )pow( 2, ( uint32_t )state->reseeds ) % state->reseeds )
            k = state->reseeds + 1;
        else
            k = state->reseeds;

        /* Ensure the index doesn't overflow. */
        k %= FORTUNA_NUM_POOLS;
    }

    /* Use the k'th pool and mix in the entropy. */
    SHA256_Update( &state->pools[ k ], digest, SHA256_DIGEST_LENGTH );

    memset( &ctx, 0, sizeof( ctx ) );
    memset( digest, 0, SHA256_DIGEST_LENGTH );
}

/**
  * Extracts entropy from the internal state and updates the key used.
  */
void fortuna_extract( struct fortuna_state* state, void* output, size_t output_length ) {
    uint8_t digest[ SHA256_DIGEST_LENGTH ];
    uint8_t block[ AES_BLOCK_SIZE ];
    uint8_t* dest;
    size_t n, t;

    assert( state != NULL );
    assert( state->seeded >= FORTUNA_MIN_SEED_CONTENT );

    assert( output != NULL );
    assert( output_length != 0 );
    assert( ( output_length % AES_BLOCK_SIZE ) < pow( 2, 20 ) );

    /**
      * We should initiate a reseed if:
      *  - We have never done one.
      *  - Enough time has passed to require a reseed.
      */
    if ( !state->reseeds || fortuna_should_reseed( state ) )
        fortuna_reseed( state );

    dest = ( uint8_t* )output;
    t = 0;

    /* Operate on requested blocks. */
    while( output_length ) {
        /* Get some entropy. */
        fortuna_update_counter( state, block );

        /* Determine if it's enough. */
        if( output_length > AES_BLOCK_SIZE )
            n = AES_BLOCK_SIZE;
        else
            n = output_length;

        /* Send it to the output. */
        memcpy( dest, block, n );

        /* Update the positions. */
        output_length -= n;
        dest += n;
        t += n;

        /* If we read too much at one time using the current key, change it. */
        if( t >= FORTUNA_MAX_READ ) {
            fortuna_update_key( state );
            t = 0;
        }
    }

    /* After every full read, update the key. */
    fortuna_update_key( state );

    memset( block, 0, sizeof( block ) );
    memset( digest, 0, sizeof( digest ) );
}